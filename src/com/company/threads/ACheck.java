package com.company.threads;

import com.company.shop.Orders;

public abstract class ACheck implements Runnable{
    protected Orders orders;

    public ACheck() {
        orders = new Orders<>();
    }

    boolean fstop=false;
    public void finish(){
        fstop=true;
    }
}
