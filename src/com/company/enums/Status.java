package com.company.enums;

public enum Status {
    WAITING,
    PROCESSED;
}
