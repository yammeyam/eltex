package com.company.enums;

public enum Title {
    MOBILEPHONE,
    SMARTPHONE,
    TABLET;
}
