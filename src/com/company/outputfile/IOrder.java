package com.company.outputfile;


import com.company.shop.Order;
import com.company.shop.Orders;

import java.util.UUID;

public interface IOrder {
    Order readById (UUID uuid);
    void saveById(UUID uuid, Orders<Order> orders);
    Orders<Order> readAll();
    void saveAll(Orders<Order> orders);
}
